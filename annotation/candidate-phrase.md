# Candidate Phrase

Candidate phrase is a sub-string of phrase.

The reason we need the candidate phrases, is because it's hard to optimize the best length for finding matching entity.

We choose N-Gram as splitting algorithm, because we had already filter by part of speech (noun and proper noun only), 
the computing performance (Big-O) is not a big issue.   
