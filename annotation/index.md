# Automatic semantic image Annotation

Linked image with relatives entities under the ontology structure:
   * (dbo:Person foaf:img dbo:Image)
   * (dbo:Image dbo:date xsd:Image)
   * (dbo:Image dbo:location dbo:Place)
   * (dbo:Image dbo:team dbo:SportsTeam)
   * ...

There are three main parts of annotation flow:
1. [Candidate phrase extraction](#candidate-phrase-extraction): Get all possible phrases from sources as follows:
    * article title
    * article content
    * image capture
   > **Note**: Of course, image should belong to the article
2. [Candidate instance identification](#candidate-phrase-identification): Mapping phrases to entities
3. [Image annotation](#image-annotation): Linked image with entity

## Candidate phrase extraction

1. [Find](https://stanfordnlp.github.io/CoreNLP/) all phrases (noun and proper noun) from sources
   > **Note:** we will group longest continued noun and proper noun as one phrase
   * **P** defined as set of all phrases we get
   * **p[i]** defined as each phrase in P
   * example (in typescript):
       ```typescript
       const caption: string = "Dec 5, 2014;Boston, MA, USA;Los Angeles Lakers guard Kobe Bryant (24) drives to the hoop against Boston Celtics forward Jeff Green (back) during the second half at TD Garden";
       // after process by Standford CoreNLP toolkit we can get result like:
       const P: string[] = [
           "Los Angeles Lakers", // p[0]
           "guard", // p[1]
           "Kobe Bryant", // p[2]
           "hoop", // p[3]
           "Boston Celtics forward Jeff Green", // p[4]
           "second half", // p[5]
           "TD Garden" // p[6]
       ];
       ```

2. Split each phrase into [candidate phrase](./candidate-phrase.md) with [N-Grams](https://kavita-ganesan.com/what-are-n-grams/#.YR6MWY4zaUk)
   * **CP** defined as set of all candidate phrases
   * **cp[i][j]** defined as each candidate phrase in CP
      > **Note:** i is the index of p[i]
   * example
       ```typescript
       const CP: string[][] = [
          [ "Los Angeles Lakers", "Los Angeles", "Angeles Lakers", "Los", "Angeles", "Lakers" ],
          [ "guard" ],
          [ "Kobe Bryant", "Kobe", "Bryant" ],
          // ...
          [ "TD Garden", "TD", "Garden" ],
       ];
       // cp[0][2] will be "Los Angeles"
       ```

## Candidate phrase identification

1. Find the subjects linked to cp[i][j]: (?s, rdfs:label, cp[i][j])
2. Find the subjects linked to different types (owl:Class, owl:Thing, dbo:Person, etc.)
3. Intersection of step 1 result and step 2 result
   > **Note:** we will stop the query when there is intersection of step 1 result and step 2 result,
   > also the intersection set has the only one entity
   * Full query combinations show as below:
      * (?s, rdfs:label, cp[i][j]) && (?s, rdfs:type, owl:Class)
      * (?s, rdfs:label, cp[i][j]) && (?s, rdfs:type, owl:Thing)
      * (?s, rdfs:label, cp[i][j]) && (?s, rdfs:type, dbo:Person)
      * (?s, rdfs:label, cp[i][j]) && (?s, rdfs:type, dbo:Place)
      * (?s, rdfs:label, cp[i][j]) && (?s, rdfs:type, dbo:SportsTeam)
      * ...
4. Filter search result with two conditions below:
   1. result should not be null
   2. the only result for longest candidate phrase in same [N-Gram](https://kavita-ganesan.com/what-are-n-grams/#.YR6MWY4zaUk) set

   * **CPU** defined as all subjects (after filtering) we found for CP
   * **cpu[i][j]** defined as result (both subject and null) we found by cp[i][j]
   * example:
       ```typescript
       const P[4] = "Boston Celtics forward Jeff Green";
       const cp[4] = [
          "Boston Celtics forward Jeff Green",
          "Boston Celtics forward Jeff",
          "Celtics forward Jeff Green",
          "Boston Celtics forward",
          "Celtics forward Jeff",
          "forward Jeff Green",
          "Boston Celtics",
          "Celtics forward",
          "forward Jeff",
          "Jeff Green",
          "Boston",
          "Celtics",
          "forward",
          "Jeff",
          "Green",
       ];
       const queryResult = [
          null,
          null,
          null,
          null,
          null,
          null,
          dbr:Boston_Celtics, // cpu[4][6]
          null,
          null,
          dbr:Jeff_Green, // cpu[4][9]
          dbr:Boston, // cpu[4][10]: it will be discarded, because cp[4][10] is sub-string of cp[4][6]
          dbr:Celtics, // cpu[4][11]: X
          dbr:Forward, // cpu[4][12]
          dbr:Jeff, // cpu[4][13]: X
          dbr:Green, // cpu[4][14]: X
       ];
       const CP[4] = [ dbr:Boston_Celtics, dbr:Jeff_Green, dbr:Forward, ];
       ```
5. Update cpu[i][j] for synonyms entity by: (cpu[i][j], dbo:wikiPageRedirects, ?o)
   > **Note:** if and only if we can find the object by query, value of cpu[i][j] will be updated as object  

6. Update cpu[i][j] for disambiguate entity:
   * query by (cpu[i][j], dbo:wikiPageDisambiguates, ?o)
   * update value of cpu[i][j] as object which has maximized relation (predicate) counts with other cpu[i][j]
   * example
       ```typescript
       let cpu[4][9] = dbr:Jeff_Green;
       const queryResult = [
          dbr:Curb_Your_Enthusiasm,
          dbr:Jeff_Green_(basketball),
          dbr:Jeff_Green_(businessman),
          dbr:Jeff_Green_(comedian),
          dbr:Jeff_Green_(multimedia_artist),
          dbr:Jeff_Green_(politician),
          dbr:Jeff_Green_(racing_driver),
          dbr:Jeff_Green_(writer),
          dbr:Jeff_Greene,
       ];
       const relationCounts = [0, 1, 0, 0, 0, 0, 0, 0, 0];
       cpu[4][9] = Jeff_Green_(basketball);
       ```

## Image Annotation

There are two types of annotation:
   * (cpu[i][j], predicate, image_URI): image as object
   * (image_URI, predicate, cpu[i][j]): image as subject

1. Image as object
   * Find all types of cpu[i][j] by: (cpu[i][j], rdf:type, ?o)
   * Find the subjects (the predicate) by query: (?s, rdf:domain, o) && (?s, rdf:range, dbo:Image)
   > **Note:** we will stop query with different types when there is result
   * Create annotation **(cpu[i][j], predicate, image_URI)** and insert into IARDF storage

2. Image as subject
   > **Note:** we will do this process only when no result from step 1 
   * Find all types of cpu[i][j] by: (cpu[i][j], rdf:type, ?o)
   * Find the subjects (the predicate) by query: (?s, rdf:domain, dbo:Image) && (?s, rdf:range, o)
   > **Note:** we will stop query with different types when there is result
   * Create annotation **(image_URI, predicate, cpu[i][j])** and insert into IARDF storage 
